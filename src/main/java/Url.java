import com.sun.tools.javac.util.ArrayUtils;

import java.util.Arrays;
import java.util.regex.Pattern;

public class Url {

    final private String url;
    final private String domain;
    final private String protocal;
    private String path;
    enum validProtocol {
        ftp,http,https,file
    }


    public Url(String url){
        this.url = url.toLowerCase();
        this.domain = getDomain();
        this.protocal = getProtocol();
        this.path = getPath();
    }

    public String getDomain() {
        // domain may only contain letters, numbers, ., -, or _
        // ex launchcode.org
        String domain = url.split("/")[2]; // recover domain
        Pattern p = Pattern.compile("[\\d|\\w|.|-|_]*"); // pattern to match

        try {
            p.matcher(domain);
            return domain;
        } catch (IllegalArgumentException exc) {
            return "Invalid domain";
        }
    }

    public String getProtocol() {
        // one of ftp, http, https, and file, non-empty

        // split the URL on the colon double slashes
        String protocol= url.split("://")[0];
        try {
            validProtocol.valueOf(protocol);
            return protocol;
        } catch (IllegalArgumentException exc) {
            return "Invalid protocol";
        }
    }

    public String getPath() {
        // may be empty
        path = "";
        String[] urlArray = url.split("/");
        for (int i=3; i<urlArray.length; i++) { // recover everything after the domain
            path = path + "/" + urlArray[i];
        }
        return path;
    }

    public String toString() {
        // https://launchcode.org/learn"
        return this.protocal + "://" + this.domain + this.path;
    }


}
