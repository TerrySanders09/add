import org.junit.Test;
import static org.junit.Assert.*;

public class TestUrl {

    // @Test( expected = IllegalArgumentException.class) on the one expected to fail

    @Test
    public void testOneUrl(){
        Url url = new Url("https://launchcode.org/learn");
        assertEquals("https://launchcode.org/learn",url.toString());
    }

    @Test
    public void testTwoUrl(){
        Url url = new Url("https://launchcode.org/learn/alot");
        assertEquals("https://launchcode.org/learn/alot",url.toString());
    }

    // one of ftp, http, https, and file
    @Test
    public void testOneProtocol() {
        Url url = new Url("ftp://launchcode.org/learn");
        assertEquals("ftp", url.getProtocol());
    }

    @Test
    public void testTwoProtocol() {
        Url url = new Url("http://launchcode.org/learn");
        assertEquals("http", url.getProtocol());
    }

    @Test
    public void testThreeProtocol() {
        Url url = new Url("https://launchcode.org/learn");
        assertEquals("https", url.getProtocol());
    }

    @Test
    public void testFourProtocol() {
        Url url = new Url("file://launchcode.org/learn");
        assertEquals("file", url.getProtocol());
    }

    @Test( expected = IllegalArgumentException.class)
    public void testFiveProtocol() {
        Url url = new Url("blah://launchcode.org/learn");
        url.getProtocol();
    }

    // domain may only contain letters, numbers, ., -, or _
    @Test
    public void testOneDomain() {
        Url url = new Url("https://launchcode.org/learn");
        assertEquals("launchcode.org",url.getDomain());
    }

    @Test
    public void testTwoDomain() {
        Url url = new Url("https://launchcode5.org/learn");
        assertEquals("launchcode5.org",url.getDomain());
    }

    @Test
    public void testThreeDomain() {
        Url url = new Url("https://launch-code.org/learn");
        assertEquals("launch-code.org",url.getDomain());
    }

    @Test
    public void testFourDomain() {
        Url url = new Url("https://launch_code.org/learn");
        assertEquals("launch_code.org",url.getDomain());
    }

    @Test( expected = IllegalArgumentException.class)
    public void testFiveDomain() {
        Url url = new Url("https://@launchcode.org/learn");
        url.getProtocol();
    }


    @Test
    public void testOnePath() {
        Url url = new Url("https://launchcode.org/learn");
        assertEquals("/learn",url.getPath());
    }

    @Test
    public void testTwoPath() {
        Url url = new Url("https://launchcode.org");
        assertEquals("",url.getPath());
    }

}
